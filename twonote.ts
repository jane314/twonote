/*
 *
 * TWONOTE by Jane Davis
 *
 */


/*
 * dependencies
 */
import { existsSync, walkSync } from 'https://deno.land/std/fs/mod.ts';
import { parse } from 'https://deno.land/std/flags/mod.ts';
import { exec, OutputMode } from 'https://deno.land/x/exec@0.0.5/mod.ts';


/*
 * configuration data
 */
const args = parse(Deno.args);
const basedir = args.b;

const config_filename = basedir + 'twonote_config.json';
if( ! existsSync( config_filename ) ) {
	console.log("Could not run. We need a twonote_config.json file in the same directory as twonote and twonote.ts");
	Deno.exit();
}
let config = JSON.parse ( Deno.readTextFileSync ( config_filename ) ) ;
const first_line = ( config.file_description == "first_line" );

const out_filename = basedir + "output.json";
if( ! existsSync( out_filename ) ) {
	Deno.writeTextFileSync( out_filename, '{}' );
}
const tmpobj = JSON.parse( Deno.readTextFileSync(out_filename) );
const out_data: any[] = Object.keys(tmpobj).map( key => tmpobj[key] );


/*
 * some necessary text parsing
 */
function has_TODO(t: string, longterm: boolean ): boolean {
	let bool = ( t.toUpperCase().search ( 'TODO' ) != -1 ) && ( t.toUpperCase().search ( 'TODONE' ) == -1 );
	if( !longterm ) {
		bool = bool && ( t.toUpperCase().search('TODOL') == -1 );
	}
	return bool
}

function dir_of_filepath ( str: string ) : string  {
	let re = new RegExp ( '(?<dir>.*/)(?<filename>[^/]*$)' ) ;
let match: any = str.match ( re );
if ( match != null ) {
	if ( 'groups' in match ) {
		if ( 'dir' in match.groups ) {
			return match.groups.dir;
		}
	}
} 
return '';
}

function filename_of_filepath ( str: string ) : string {
	let re = new RegExp ( '(?<dir>.*\/)(?<filename>[^/]*$)' ) ;
	let match: any = str.match ( re );
	if ( match != null ) {
		if ( 'groups' in match ) {
			if ( 'dir' in match.groups ) {
				return match.groups.filename.toString() ;
			}
		}
	} 
	return '';
}


/*
 * update and quit if have -u flag
 */
if ( args.hasOwnProperty('u') ) {
	let newconfig: any = JSON.parse(Deno.readTextFileSync('/tmp/twonotenewconfig.json'));
	let newkeys: string[] = [];

	if( config.version == newconfig.version ) {
		console.log(`You are using the current version: TwoNote ${config.version}.`);
		Deno.exit();
	} else {
		console.log(`Updating to TwoNote ${newconfig.version}.`);
	}

	Object.keys(newconfig).forEach( key => {
		if( !config.hasOwnProperty(key) ) {
			config[key] = newconfig[key];
			newkeys.push(key);
		}
	});

	config.version = newconfig.version;
	Deno.writeTextFileSync('twonote_config.json', JSON.stringify(config, null, 4));

	if( newkeys.length != 0 ) {
		console.log("\nThe following new options were added to twonote_config.json. See https://gitlab.com/jane314/twonote for more information.\n");
		console.log(newkeys.join('\n'));
		console.log("\n");
	} 
	Deno.exit();

}

/*
 *
 * main functionality
 *
 */

// first mark any items done
if ( args.hasOwnProperty('d') ) {
	if( out_data.hasOwnProperty(args.d) ) {
		if( out_data[args.d].hasOwnProperty('path') && out_data[args.d].hasOwnProperty('linenum') ) {
			const cmd1 = `perl -pi -e "s/TODO/TODONE/ if $. == ${out_data[args.d].linenum}" "${out_data[args.d].path}"`;
			let tmp = await exec( cmd1, {output: OutputMode.Capture} ); 
		}
	}
}

function output_table(work_dir: string, first_line: boolean, clear_screen: boolean ): void {

	let notebook_description: string = "";
	let table: any = [];

	if ( clear_screen ) {
		console.clear();	
	}

	for ( const entry of walkSync ( work_dir ) ) {

		if ( entry.isDirectory ) {
			let tmp_filepath = entry.path + '/info.json';
			if ( existsSync ( tmp_filepath ) ) {
				notebook_description = JSON.parse ( Deno.readTextFileSync ( tmp_filepath ) ).name;
			}
			continue;
		} 

		if ( entry.isFile ) {
			if ( filename_of_filepath ( entry.path ) == 'info.json' ) {
				continue;
			}
		}

		let text = Deno.readTextFileSync ( entry.path ) ;
		let file_description: string;

		if( first_line) {
			file_description = text.substring(0, text.search('\n'));
		} else {
			file_description = filename_of_filepath( entry.path );
		}

		if ( text.search ( 'TODO' ) != -1 ) {
			let logitems = text.split ( '\n' ).forEach ( 
														( logitem: string, index: number ) => {
															if( has_TODO(logitem, args.hasOwnProperty('l') ) ) {
																table.push( { "Notebook": notebook_description, "File":file_description, "TODO Item":logitem.replace(/^\s*/, ""), "path":entry.path, "linenum":index+1 } );
															}
														} 
													   );
		}
	}

	console.table(table, ["Notebook", "File", "TODO Item"]);
	Deno.writeTextFileSync( out_filename, JSON.stringify({...table}) );

}

/*
 *
 * parse config and run main functionality
 *
 */
if ( args.hasOwnProperty('n') ) {
	if( !existsSync(out_filename) ) {
		console.log("No output.json file. Run \"twonote\" again before using the -n flag.");
	} else {
		if( args.n > out_data.length - 1 ) {
			console.log("No such index n = ", args.n, " in output.json");
		} else {
			exec("vim \"" + out_data[args.n].path + "\"");	
		}
	}
} else if( args.hasOwnProperty('c') ) {
	output_table( args.c, first_line, config.clear_screen );
} else {
	output_table( config.directory, first_line, config.clear_screen );
}



