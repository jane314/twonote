/*
 *
 * TWONOTE by Jane Davis
 *
 */


/*
 * dependencies
 */

import { existsSync, walkSync } from 'https://deno.land/std/fs/mod.ts';
import { parse } from 'https://deno.land/std/flags/mod.ts';
import { exec, OutputMode } from 'https://deno.land/x/exec@0.0.5/mod.ts';


/*
 * types
 */

// whether line has TODO, TODOL, TODATE, or none
enum TextLineStatus {
	NONE,
		TODO,
		TODOL,
		TODONE,
		TODATE
};


/*
 * configuration data
 */

const args = parse(Deno.args);
const basedir = args.b;
const config_filename = basedir + 'twonote_config.json';
if( ! existsSync( config_filename ) ) {
	console.log("Could not run. We need a twonote_config.json file in the same directory as twonote and twonote.ts");
	Deno.exit();
}
let config = JSON.parse ( Deno.readTextFileSync ( config_filename ) ) ;
const first_line = ( config.file_description == "first_line" );
const out1_filename = basedir + "output1.json";
if( ! existsSync( out1_filename ) ) {
	Deno.writeTextFileSync( out1_filename, '{}' );
}
const tmpobj1 = JSON.parse( Deno.readTextFileSync(out1_filename) );
const out1_data: any[] = Object.keys(tmpobj1).map( key => tmpobj1[key] );
const out2_filename = basedir + "output2.json";
if( ! existsSync( out2_filename ) ) {
	Deno.writeTextFileSync( out2_filename, '{}' );
}
const tmpobj2 = JSON.parse( Deno.readTextFileSync(out2_filename) );
const out2_data: any[] = Object.keys(tmpobj2).map( key => tmpobj2[key] );


/*
 * some necessary text parsing
 */

function has_TODO(t: string ): TextLineStatus {

	let text = t.toUpperCase();

	if( text.search( 'TODOL ') != -1 ) {
		return TextLineStatus.TODOL;
	} else if( text.search( 'TODONE' ) != -1 ) {
		return TextLineStatus.TODONE;
	} else if( text.search( 'TODATE' ) != -1 ) {
		return TextLineStatus.TODATE;
	} else if( text.search( 'TODO' ) != -1 ) {
		return TextLineStatus.TODO;
	} else {
		return TextLineStatus.NONE;
	}

}


function dir_of_filepath ( str: string ) : string  {
	let re = new RegExp ( '(?<dir>.*/)(?<filename>[^/]*$)' ) ;
	let match: any = str.match ( re );
	if ( match != null ) {
		if ( 'groups' in match ) {
			if ( 'dir' in match.groups ) {
				return match.groups.dir;
			}
		}
	} 
	return '';
}

function filename_of_filepath ( str: string ) : string {
	let re = new RegExp ( '(?<dir>.*\/)(?<filename>[^/]*$)' ) ;
	let match: any = str.match ( re );
	if ( match != null ) {
		if ( 'groups' in match ) {
			if ( 'dir' in match.groups ) {
				return match.groups.filename.toString() ;
			}
		}
	} 
	return '';
}


/*
 * update and quit if have -u flag
 */

if ( args.hasOwnProperty('u') ) {
	let newconfig: any = JSON.parse(Deno.readTextFileSync('/tmp/twonotenewconfig.json'));
	let newkeys: string[] = [];

	if( config.version == newconfig.version ) {
		console.log(`You are using the current version: TwoNote ${config.version}.`);
		Deno.exit();
	} else {
		console.log(`Updating to TwoNote ${newconfig.version}.`);
	}

	Object.keys(newconfig).forEach( key => {
		if( !config.hasOwnProperty(key) ) {
			config[key] = newconfig[key];
			newkeys.push(key);
		}
	});

	config.version = newconfig.version;
	Deno.writeTextFileSync('twonote_config.json', JSON.stringify(config, null, 4));

	if( newkeys.length != 0 ) {
		console.log("\nThe following new options were added to twonote_config.json. See https://gitlab.com/jane314/twonote for more information.\n");
		console.log(newkeys.join('\n'));
		console.log("\n");
	} 

	Deno.exit();

}


/*
 *
 * main functionality
 *
 */

// first mark any items done
if ( args.hasOwnProperty('d') ) {
	if( args.hasOwnProperty('l') ) {
		if( out2_data.hasOwnProperty(args.d) ) {
			if( out2_data[args.d].hasOwnProperty('path') && out2_data[args.d].hasOwnProperty('linenum') ) {
				const cmd2 = `perl -pi -e "s/TODO/TODONE/ if $. == ${out2_data[args.d].linenum}" "${out2_data[args.d].path}"`;
				let tmp = await exec( cmd2, {output: OutputMode.Capture} ); 
			}
		}		
	} else {
		if( out1_data.hasOwnProperty(args.d) ) {
			if( out1_data[args.d].hasOwnProperty('path') && out1_data[args.d].hasOwnProperty('linenum') ) {
				const cmd1 = `perl -pi -e "s/TODO/TODONE/ if $. == ${out1_data[args.d].linenum}" "${out1_data[args.d].path}"`;
				let tmp = await exec( cmd1, {output: OutputMode.Capture} ); 
			}
		}
	}
}

function output_table(work_dir: string, first_line: boolean, clear_screen: boolean ): void {

	let notebook_description: string = "";
	let table1: any = [];
	let table2: any = [];

	if ( clear_screen ) {
		console.clear();	
	}

	for ( const entry of walkSync ( work_dir ) ) {

		if ( entry.isDirectory ) {
			let tmp_filepath = entry.path + '/info.json';
			if ( existsSync ( tmp_filepath ) ) {
				notebook_description = JSON.parse ( Deno.readTextFileSync ( tmp_filepath ) ).name;
			}
			continue;
		} 

		if ( entry.isFile ) {
			if ( filename_of_filepath ( entry.path ) == 'info.json' ) {
				continue;
			}
		}

		let text = Deno.readTextFileSync ( entry.path ) ;
		let file_description: string;

		if( first_line) {
			file_description = text.substring(0, text.search('\n'));
		} else {
			file_description = filename_of_filepath( entry.path );
		}

		if ( text.search ( 'TOD' ) != -1 ) {
			let logitems = text.split ( '\n' ).forEach ( 
				( logitem: string, index: number ) => {
					let status = has_TODO(logitem);
					if ( status == TextLineStatus.TODO ) {
						table1.push( { "Notebook": notebook_description, "File":file_description, "TODO Item":logitem.replace(/^\s*/, ""), "path":entry.path, "linenum":index+1 } );
					} else if ( status == TextLineStatus.TODOL && args.hasOwnProperty('l') ) {
						table2.push( { "Notebook": notebook_description, "File":file_description, "TODO Item":logitem.replace(/^\s*/, ""), "path":entry.path, "linenum":index+1 } );
					} else {}				 
			} );
		}

		console.table(table1, ["Notebook", "File", "TODO Item"]);
		
		if( args.hasOwnProperty('l') ) {
			console.log("\nLong Term Items:\n");
			console.table(table2, ["Notebook", "File", "TODO Item"]);
		}
		
		Deno.writeTextFileSync( out1_filename, JSON.stringify({...table1}) );
		Deno.writeTextFileSync( out2_filename, JSON.stringify({...table2}) );

	}
}

	/*
	 *
	 * parse config and run main functionality
	 *
	 */

	if ( args.hasOwnProperty('n') ) {
		if( !existsSync(out_filename) ) {
			console.log("No output.json file. Run \"twonote\" again before using the -n flag.");
		} else {
			if( args.n > out_data.length - 1 ) {
				console.log("No such index n = ", args.n, " in output.json");
			} else {
				exec("vim \"" + out_data[args.n].path + "\"");	
			}
		}
	} else if( args.hasOwnProperty('c') ) {
		output_table( args.c, first_line, config.clear_screen );
	} else {
		output_table( config.directory, first_line, config.clear_screen );
	}



