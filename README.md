# TwoNote

My command-line replacement for OneNote, using [Deno](https://deno.land/). Tested on CentOS and Ubuntu.

To use this, we specify a notes directory in the `twonote_config.json` file which contains our text files with "TODO" items.

Running `twonote` prints all lines in text files under the notes directory that contain "TODO" but not "TODONE".

This is my way of managing my personal customer call logs. I put "TODO" items in my log files, and mark a TODO complete by changing it to TODONE. This script helps me keep track of outstanding items.

Think of the whole notes directory as the OneNote app, and each subdirectory as a OneNote notebook.

=========================================================

Dependencies and Setup:
- Make sure you can run the `perl`, `vim`, `realpath`, `curl`, and `jq` commands from the Bash prompt. If not, install the ones you need with your package manager.
- Install [Deno](https://deno.land/) and make sure you can run the `deno` command from the command line.
- Download this repository and place `twonote`, `twonote.ts`, and `twonote_config.json` together in a folder in your `PATH` variable. Give `twonote` permission to execute.
- Specify your notes directory in `twonote_config.json`. (Use an absolute path, not a relative one.) An example notes directory is provided in the `example` folder in this repository.
- After this, you should be good to go! Make lots of folders inside your notes directory, and log lots of text files with TODO items.

Command Line Usage and Options:
- Usage: `twonote [OPTIONS]`.

| Flags | Description |
| --- | --- |
| No flags | Displays your outstanding TODO items in your notes directory |
| `-b`, `--browse` | Takes you to your notes directory in Vim browsing mode, as in the screenshot below. |
| `-c`, `--cur`, `--current` | Overrides the directory set in `twonote_config.json` and uses the current working directory as the notes directory. |
| `-d XXX`, `--done=XXX` | Marks the TODO item with index number XXX as done. The index number is according to the most recent run of a standard `twonote` command. |
| `-h` | `--help` | Displays help info.
| `-l` and `--longterm` | Includes TODOL items in the table of TODO items.
| `-n XXX`, `--num=XXX`, `--number=XXX` | Opens Vim to edit TODO item with index number XXX. The index number is according to the most recent run of a standard `twonote` command. |
| `-u`, `--up`, `twonote --update` | Updates TwoNote. |
- You can do some filtering by piping to `grep`! For example, `twonote | grep -i grocery`.

`twonote_config.json` Settings:

| Setting | Description |
| --- | --- |
| `clear_screen` | Can be set to 1 or 0. This controls whether or not the console should be cleared before the TODO summary table is printed. |
| `directory` | Your notes directory. Use an absolute path here. |
| `file_description` | Can be set to either `first_line` or `file_name`. This controls the output of the second column in the TODO summary table, whether you want each note file to be referred to by its filename or title line. |
| `version` | Version number. Don't change this! The update mechanism uses this. |

Notes:
- This is designed for Linux environments. I tested this in [WSL](https://docs.microsoft.com/en-us/windows/wsl/).
- The `twonote` executable file must remain in the same directory as `twonote_config.json` and `twonote.ts`. 
- In your notes files, only put one TODO or TODONE per line!
- In each subdirectory of your notes directory, you can put an `info.json` file with a `name` attribute that specifies the subdirectory's name. See the `example` folder here for reference. 

=========================================================

Demo:

- The `twonote_config.json` file, the notes directory, and the subsequent output of `twonote`:

![Notes dir](01.png)

- Browse Mode:

![Browse](02.png)
